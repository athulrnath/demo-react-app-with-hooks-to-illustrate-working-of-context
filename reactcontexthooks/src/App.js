import React from "react";
import SongList from "./components/songList";
import BookList from "./components/bookList";
import Navbar from "./components/navbar";
import ThemeContextProvider from "./contexts/themeContext";
import AuthContextProvider from "./contexts/authContext";
import ThemeToggle from "./components/themeToggle";
import AuthToggle from "./components/authToggle";

function App() {
  return (
    <div className="App">
      <AuthContextProvider>
        <ThemeContextProvider>
          <SongList />
          <BookList />
          <Navbar />
          <ThemeToggle />
          <AuthToggle />
        </ThemeContextProvider>
      </AuthContextProvider>
    </div>
  );
}

export default App;
