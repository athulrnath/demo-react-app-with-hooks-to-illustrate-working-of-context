import React, { createContext, useState } from "react";
export const authContext = createContext();

const AuthContextProvider = props => {
  const [isAuthenticated, setIsAuthenticated] = useState(true);

  //function to change auth context
  const toggleIsAuthenticated = () => {
    setIsAuthenticated(isAuthenticated => (isAuthenticated = !isAuthenticated));
  };
  return (
    <authContext.Provider value={{ isAuthenticated, toggleIsAuthenticated }}>
      {props.children}
    </authContext.Provider>
  );
};

export default AuthContextProvider;
