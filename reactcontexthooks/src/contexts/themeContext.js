import React, { createContext, useState } from "react";

export const themeContext = createContext();
const ThemeContextProvider = props => {
  const [theme, setTheme] = useState({
    isLight: true,
    light: { bg: "#eee", ui: "#ggg", syntax: "#555" },
    dark: { bg: "#555", ui: "#333", syntax: "#ddd" }
  });
  //function to change theme context
  const toggleTheme = () => {
    setTheme(prevTheme => ({ ...prevTheme, isLight: !prevTheme.isLight }));
    console.log(theme);
  };
  return (
    <themeContext.Provider value={{ ...theme, toggleTheme: toggleTheme }}>
      {props.children}
    </themeContext.Provider>
  );
};

export default ThemeContextProvider;
