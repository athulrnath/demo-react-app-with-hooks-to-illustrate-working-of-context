import React, { useContext } from "react";
import { themeContext } from "../contexts/themeContext";
import { authContext } from "../contexts/authContext";

const BookList = () => {
  //importing authcontext and theme context into booklist component
  const theme = useContext(themeContext);
  const auth = useContext(authContext);

  //console logging the current value of auth and them context
  console.log("Theme context is ", theme);
  console.log("Auth context is", auth);

  const currentTheme = theme.isLight ? theme.light : theme.dark;

  //current theme for list elements
  console.log("current theme is", currentTheme);
  return (
    <div className="BookList">
      <ul>
        <li style={{ background: currentTheme.bg }}>
          Mindfulness in plain english
        </li>
        <li style={{ background: currentTheme.bg }}>Total recall</li>
        <li style={{ background: currentTheme.bg }}>My story</li>
      </ul>
    </div>
  );
};

export default BookList;
