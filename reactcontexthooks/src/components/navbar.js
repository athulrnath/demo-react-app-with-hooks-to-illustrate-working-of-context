import React, { useContext } from "react";
import { themeContext } from "../contexts/themeContext";
import { authContext } from "../contexts/authContext";
const Navbar = () => {
  //console logging the current value of auth and theme context
  const theme = useContext(themeContext);
  const auth = useContext(authContext);

  //console logging the current value of auth and theme context
  console.log("Theme context is ", theme);
  console.log("Auth context is", auth);

  //current theme for navbar
  const currentTheme = theme.isLight ? theme.light : theme.dark;
  console.log("Current Theme is", currentTheme);
  console.log(auth);
  return (
    <nav style={{ background: currentTheme.bg }}>
      <h1>Context App</h1>
      <ul>
        <li>Home</li>
        <li>About</li>
        <li>Contact</li>
      </ul>
    </nav>
  );
};

export default Navbar;
