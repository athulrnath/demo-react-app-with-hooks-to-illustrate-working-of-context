import React, { useState, useEffect, useContext } from "react";
import NewSongForm from "./addSong";
import { themeContext } from "../contexts/themeContext";
import { authContext } from "../contexts/authContext";
const SongList = () => {
  //usestate function returns an array whose first element is the state data and the second element is a function to modify the state data
  const [songs, setSongs] = useState([
    { name: "hotel california", band: "eagles", id: 1 },
    { name: "Highway to Hell", band: "acdc", id: 2 },
    { name: "Dark Necessities", band: "rhcp", id: 3 },
    { name: "Take it Easy", band: "Eagles", id: 4 }
  ]);
  const [age, setAge] = useState(20);
  useEffect(() => {
    console.log("useEffect hook triggered", songs);
  }, [songs]);
  useEffect(() => {
    console.log("useEffect hook triggered", age);
  }, [age]);

  const addSong = value => {
    setSongs(prevsongs =>
      prevsongs.concat({
        name: value,
        band: "kk",
        id: prevsongs.length + 1
      })
    );
  };
  const theme = useContext(themeContext);
  const auth = useContext(authContext);

  //console logging the current value of auth and them context
  console.log("Theme context is ", theme);
  console.log("Auth context is", auth);

  const currentTheme = theme.isLight ? theme.light : theme.dark;
  return (
    <div>
      <ul>
        {songs.map(song => (
          <li key={song.id} style={{ background: currentTheme.bg }}>
            {song.name}
          </li>
        ))}
      </ul>
      <NewSongForm addSong={addSong} />
      <button
        onClick={() => {
          setAge(age => age + 1);
        }}
      >
        Add age by 1 {age}
      </button>
    </div>
  );
};

export default SongList;
