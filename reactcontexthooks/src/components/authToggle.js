import React, { useContext } from "react";
import { authContext } from "../contexts/authContext";

//button to show the working of auth context
const AuthToggle = () => {
  //importing authContext for use in authToggle component
  const auth = useContext(authContext);

  return (
    <button onClick={auth.toggleIsAuthenticated}>
      Toggle auth value {JSON.stringify(auth.isAuthenticated)}
    </button>
  );
};

export default AuthToggle;
