import React, { useContext } from "react";
import { themeContext } from "../contexts/themeContext";

const ThemeToggle = () => {
  const context = useContext(themeContext);
  return <button onClick={context.toggleTheme}>Toggle the Theme</button>;
};

export default ThemeToggle;
